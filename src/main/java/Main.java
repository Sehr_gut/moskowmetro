import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONObject;


public class Main {

  public static String link = "https://www.moscowmap.ru/metro.html#lines";

  public static void main(String[] args){

    String filePath = "sample/metro.json";

    String linesStr = "lines";
    String stationsStr = "stations";
    String connectionsStr = "Connections";

    JSONObject object = new JSONObject();
    object.put(connectionsStr, MoscowMetro.parseConnections(link));
    object.put(stationsStr, MoscowMetro.parseStations(link));
    object.put(linesStr, MoscowMetro.parseLines(link));



    File file = new File(filePath);
    FileWriter fr = null;
    try {
      fr = new FileWriter(file);

      ObjectMapper mapper = new ObjectMapper();
      Object json = mapper.readValue(object.toString(), Object.class);
      String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);

      fr.write(indented);
    } catch (IOException e) {
      e.printStackTrace();
    }finally{
      try {
        fr.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }


    ParsingFile.parceStationOnEveryLine(filePath);


  }



}
