import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class MoscowMetro {

  public static JSONArray parseLines(String link){

    List<String> linesNumbers = new ArrayList<>();
    List<String> linesNames = new ArrayList<>();

    String searchLinesNumber = "div";
    String attributeValueNumberLine = "data-depend";

    String numberStr = "number";
    String nameStr = "name";

    try {
      Document doc = Jsoup.connect(link).get();

      Elements elements = doc.select(searchLinesNumber);

      elements.forEach(element -> {
        String line = element.attr(attributeValueNumberLine);

        if (!line.equals("")){
          linesNumbers.add(line.substring(line.lastIndexOf("lines-") + 6, line.lastIndexOf("'}")));
          linesNames.add(element.text());
        }
      });
    } catch (IOException e){
      e.printStackTrace();
    }

      JSONArray arrLines = new JSONArray();

      for (int i = 0; i < linesNumbers.size(); i++) {
        JSONObject line = new JSONObject();
        arrLines.put(line.put(numberStr, linesNumbers.get(i)));
        arrLines.put(line.put(nameStr, linesNames.get(i)));

        if (i != 0) {
          if (arrLines.get(i) == arrLines.get(i - 1)) {
            arrLines.remove(i);
          }
        }
      }

      arrLines.remove(17);
      return arrLines;
  }

  public static JSONObject parseStations(String link) {

    String searchLinesNumber = "div";
    String attributeValueStationName = "data-line";

    List<String> stationsNumbers = new ArrayList<>();
    List<String> stationsNames = new ArrayList<>();


    try {
      Document doc = Jsoup.connect(link).get();
      Elements elements = doc.select(searchLinesNumber);
      elements.forEach(element -> {

        String lineSt = element.attr(attributeValueStationName);

        if (!lineSt.equals("")) {
          stationsNumbers.add(lineSt);
          stationsNames.add(element.text());
        }
      });

    } catch (IOException e){
      e.printStackTrace();
    }



    JSONObject objSt = new JSONObject();

    for (int i = 0; i < stationsNames.size(); i++){
      JSONArray arrEveryStation = new JSONArray();

      for (String everyStation : stationsNames.get(i).substring(3).split("\\d+\\.")){
        arrEveryStation.put(everyStation);

      }

        objSt.put(stationsNumbers.get(i), arrEveryStation);


    }


  return objSt;
  }


  public static JSONArray parseConnections(String link){

    String searchLinesNumber = "div";
    String attributeValueNumberLine = "data-line";
    String isConnection = "переход";
    String fromNameStart = "<span class=\"name\">";
    String fromNameEnd = "</span>";
    String lineStr = "line";
    String stationStr = "station";
    String fromStationNumber = "";

    List<String> lines = new ArrayList<>();
    List<String> sortedLines = new ArrayList<>();

    JSONArray arrFinalConnections = new JSONArray();

    try{
      Document doc = Jsoup.connect(link).get();
      Elements elements = doc.select(searchLinesNumber);
      elements.forEach(element -> {
        String line = element.attr(attributeValueNumberLine);
        if (!line.equals("")){
          lines.add(element.toString());
        }
      });

    } catch (IOException e){
      e.printStackTrace();
    }

    for (String string : lines) {
      for (String st : string.split("<p>")) {
        sortedLines.add(st);
      }

      String fromStationNumberStrStart = "data-line=\"";
      String fromStationNumberStrEnd = "\"";

      int fromStationNumberIndexStart =
          string.indexOf(fromStationNumberStrStart) + fromStationNumberStrStart.length();
      int fromStationNumberIndexEnd = string
          .indexOf(fromStationNumberStrEnd, fromStationNumberIndexStart);

      fromStationNumber = string
          .substring(fromStationNumberIndexStart, fromStationNumberIndexEnd);

    }

    for (String string : sortedLines) {
      if (string.contains(isConnection)) {

        int fromNameStartIndex = string.indexOf(fromNameStart) + fromNameStart.length();
        int fromNameEndIndex = string.indexOf(fromNameEnd, fromNameStartIndex);

        JSONObject fromConnection = new JSONObject();
        JSONArray arrConnections = new JSONArray();
        JSONArray arrToConnections = new JSONArray();
        JSONArray arrManyConnections = new JSONArray();

        String fromStationName = string
            .substring(fromNameStartIndex, fromNameEndIndex);

        fromConnection.put(lineStr, fromStationNumber);
        fromConnection.put(stationStr, fromStationName);

        arrToConnections = connections(string, arrManyConnections);

        arrConnections.put(fromConnection);
        arrToConnections.forEach(arrConnections::put);

        arrFinalConnections.put(arrConnections);

      }
    }
      return filterConnections(arrFinalConnections);
  }

  public static  JSONArray filterConnections(JSONArray arr){
    List<String> sortedStationsOfOneConnection = new ArrayList<>();
    JSONArray arrJ = new JSONArray();
    arr.toList().forEach(oneConnection -> {
      List<String> stationsOfOneConnection = new ArrayList<>();

      for (String st : oneConnection.toString().split("}, \\{")) {
        stationsOfOneConnection.add(st.replaceAll("[]\\[}{]", ""));
      }
      Collections.sort(stationsOfOneConnection);
      List<String> allStations = new ArrayList<>();
      allStations.add(stationsOfOneConnection.toString());

      allStations.forEach(connection -> {
        if (!sortedStationsOfOneConnection.contains(connection)){
          sortedStationsOfOneConnection.add(connection);
          JSONArray array = new JSONArray();
          arrJ.put(fromListToJsonArray(connection, array));

        }
      });
    });
    return arrJ;
  }


  public static JSONArray fromListToJsonArray(String string, JSONArray array){

    String lineStr = "line=";
    String stationStr = "station=";
    String stationName = ",";

    JSONObject obj = new JSONObject();

    int lineStartIndex = string.indexOf(lineStr) + lineStr.length() ;
    int lineEndIndex = string.indexOf(stationName, lineStartIndex);
    int stationStartIndex = string.indexOf(stationStr) + stationStr.length();


    String lineNumber = string.substring(lineStartIndex, lineEndIndex);
    String stName;
    if (string.substring(string.indexOf(stationStr)).contains(stationName)) {
      int stationEndIndex = string.indexOf(stationName, stationStartIndex);
      stName = string.substring(stationStartIndex, stationEndIndex);
    } else {
      stName = string.substring(stationStartIndex);
    }


    String otherSt = string.substring(string.indexOf(stationStr) + stationStr.length());

    obj.put(lineStr.replaceAll("=", ""), lineNumber);
    obj.put(stationStr.replaceAll("=", ""), stName);
    array.put(obj);

    if (otherSt.contains(lineStr)) {
      fromListToJsonArray(otherSt, array);
    }

    return array;

  }



  public static JSONArray connections(String string, JSONArray arrayToConnection){

      JSONObject toConnection = new JSONObject();

      String toNumberStart = "ln-";
      String toNumberEnd = "\"";
      String toNameStart = "\"переход на станцию «";
      String toNameEnd = "»";
      String lineStr = "line";
      String stationStr = "station";

      int toNumberStartIndex = string.indexOf(toNumberStart) + toNumberStart.length();
      int toNumberEndIndex = string.indexOf(toNumberEnd, toNumberStartIndex);
      int toNameStartIndex = string.indexOf(toNameStart) + toNameStart.length();
      int toNameEndIndex = string.indexOf(toNameEnd, toNameStartIndex);

      String toStationNumber = string.substring(toNumberStartIndex, toNumberEndIndex);
      String toStationName = string.substring(toNameStartIndex, toNameEndIndex);

      String otherSt = string.substring(toNameEndIndex);

      toConnection.put(lineStr, toStationNumber);
      toConnection.put(stationStr, toStationName);
      arrayToConnection.put(toConnection);


      if (otherSt.contains(toNameStart)) {
        connections(otherSt, arrayToConnection);
      }

      return arrayToConnection;

    }
}
