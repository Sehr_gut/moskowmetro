import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ParsingFile {

  public static void parceStationOnEveryLine(String file){

    String stationsStr = "stations";

    try {
      JSONParser parser = new JSONParser();
      JSONObject jsonData = (JSONObject) parser.parse(getJsonFile(file));

      JSONObject stationsObject = (JSONObject) jsonData.get(stationsStr);

      parseStations(stationsObject);

    } catch (Exception e){
      e.printStackTrace();
    }
  }

  private static void parseStations(JSONObject stationsObject)
  {
    int countLines = 0;


//    System.out.println(stationsObject);

    for (Object obj : stationsObject.keySet()){
      countLines++;
      int countStations = 0;
      JSONArray stationsArray = (JSONArray) stationsObject.get(obj);
      for (Object obj1 : stationsArray){
        countStations++;
      }
      System.out.println(countLines + " " + countStations);
    }


  }

  private static String getJsonFile(String dataFile){
    StringBuilder builder = new StringBuilder();
    try {
      List<String> lines = Files.readAllLines(Paths.get(dataFile));
      lines.forEach(line -> builder.append(line));
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return builder.toString();
  }
}
