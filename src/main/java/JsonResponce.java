
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class JsonResponce {

  private Map<String, JSONObject> stations = new HashMap<>();
  private Map<String, List<JSONObject>> lines = new HashMap<>();
  private Map<String, JSONArray> connections = new HashMap<>();

  public Map<String, JSONObject> getStations() {
    return stations;
  }

  public void setStations(Map<String, JSONObject> stations) {
    this.stations = stations;
  }

  public Map<String, List<JSONObject>> getLines() {
    return lines;
  }

  public void setLines(Map<String, List<JSONObject>> lines) {
    this.lines = lines;
  }

  public Map<String, JSONArray> getConnections() {
    return connections;
  }

  public void setConnections(Map<String, JSONArray> connections) {
    this.connections = connections;
  }
}